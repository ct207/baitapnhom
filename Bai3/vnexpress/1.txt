Ký ức Hội An và hành trình kể câu chuyện Việt ra thế giới
Với sự tham gia của 500 diễn viên trên sân khấu ngoài trời rộng 25.000m2, show diễn gây tiếng vang với du khách và truyền thông quốc tế.

"Đẹp" và "mãn nhãn" là những điều nhiều khán giả bày tỏ sau khi xem hình ảnh show diễn Ký ức Hội An được chiếu tại quảng trường Thời đại (Times Square), New York, Mỹ ngày 14/3 vừa qua.

Màn trình chiếu này nằm trong chiến dịch quảng cáo quy mô nhằm quảng bá cho show diễn, đưa "Ký ức Hội An" vượt ra khỏi khuôn khổ con sông Thu Bồn, ra khỏi biên giới Việt Nam để đến với công chúng quốc tế.

Mức đầu tư ngang 3 khách sạn 5 sao

Công diễn lần đầu ngày 18/3/2018, Ký ức Hội An là một chương trình biểu diễn nghệ thuật thực cảnh do do Gami Theme Park đầu tư, thể hiện trên sân khấu rộng 25.000m2, với bối cảnh sông nước tự nhiên của hòn đảo giữa sông Thu Bồn, cùng hệ thống âm thanh, ánh sáng ngoài trời đẳng cấp quốc tế.

Thông qua nhiều hình thức kể chuyện công phu bằng âm nhạc, ánh sáng, con người, cảnh vật, show diễn khắc họa quá trình hình thành, phát triển xứ Faifo - nơi từng được mệnh danh là thương cảng nhộn nhịp nhất Đông Nam Á. Người xem được thưởng thức những câu chuyện nối tiếp nhau mang tính nhân văn, lãng mạn và đầy khí chất của con người xứ Quảng.

Quá trình xây nhà lập ấp, mở mang điền địa, xây dựng đời sống, hội nhập văn hóa quốc tế tại thương cảng và gìn giữ nét đẹp của đô thị cổ Hội An được khắc họa qua 5 màn: hồi sinh, đám cưới, đèn và biển, hội nhập và áo dài được cô gái dệt vải bên khung cửi kể lại bên dòng thời gian bất tận.

Show diễn tái hiện trên sân khấu quy mô 25.000m2.
Show diễn tái hiện trên sân khấu quy mô 25.000m2.

Show diễn thực cảnh này thu hút sự tham gia của 500 diễn viên chuyên nghiệp được tuyển chọn từ Hà Nội, TP HCM và Đà Nẵng, được biên đạo múa quốc tế đào tạo nửa năm trước khi chính thức ra sân khấu.

Theo đơn vị sản xuất, hàng trăm tấn thiết bị âm thanh, ánh sáng ngoài trời hiện đại đã nhập khẩu về Việt Nam, do các kỹ sư hàng đầu vận hành và hướng dẫn sử dụng. Hiệu ứng được sử dụng nhịp nhàng nhằm nổi bật câu chuyện trong show diễn. Ví dụ, trong cảnh 100 cô gái Việt mặc áo dài đi trên một con đường - biểu trưng cho dòng thời gian đã qua của Hội An, màu sắc của chiếc áo dài liên tục biến đổi tạo nên những ấn tượng bí ẩn, điều mọi người thường ít thấy ở áo dài.

Theo chủ đầu tư Gami Theme Park, với mức đầu tư tương đương 3 khách sạn 5 sao trên diện tích 2,5ha, doanh nghiệp xác định để đạt lợi nhuận cần lộ trình lâu dài.

"Lượng khách trung bình mỗi ngày 1.000 người trên khán đài chưa đem lại doanh thu đáng kể cho Ký ức Hội An, nhưng sự gia tăng mạnh mẽ từ con số 20 – 50 người mỗi tối lên 500 và 1.000, có những đêm diễn kín khán đài 3.300 chỗ đã chứng minh giá trị của sản phẩm", chủ đầu tư nói.

Tổng cộng, cho đến nay show diễn đã thu hút 500.000 người xem, đưa Ký ức Hội An trở thành chương trình giải trí đầu tiên vừa ra mắt tại Việt Nam được tổ chức Guiness ghi nhận kỷ lục là chương trình biểu diễn nghệ thuật thường nhật có số lượng diễn viên đông nhất và kỷ lục sân khấu biểu diễn ngoài trời lớn nhất.

Show diễn quy mô do người Việt sản xuất.
Show diễn quy mô do người Việt sản xuất.

Giáo sư, Tiến sĩ khoa học Vũ Minh Giang - Phó Giám đốc Đại học Quốc gia Hà Nội nhận xét: "Một sân khấu hoành tráng như thế này tạo cho người ta cảm xúc Hội An không hề bé nhỏ. Nó có một tầm vóc, một kích cỡ mà ta có thể nhìn thấy, vô cùng rộng lớn".

"Lịch sử của thành phố cổ Hội An, trong đó có sự hội nhập và giao thoa của những nền văn hóa đa dạng tại đây như Việt Nam, Nhật Bản, Bồ Đào Nha, Pháp, Trung Quốc... được mô tả rất tinh tế và thú vị. Tôi đã từng được xem rất nhiều show diễn tại các quốc gia khác, nhưng Ký ức Hội An là show diễn tuyệt vời nhất mà tôi từng thưởng thức", đại sứ Cộng hòa Séc tại Việt Nam, ông Grepl Vitezslav nói.

Show diễn Việt Nam trên quảng trường Thời đại

Đơn vị sản xuất Gami Theme Park xác định, muốn quảng bá văn hóa Việt Nam ra thế giới phải sử dụng công nghệ quốc tế để khắc họa những nét vàng son của dân tộc. Cùng với đó, biểu diễn nghệ thuật thực cảnh là loại hình nghệ thuật được sinh ra để khắc họa, tôn vinh nét đẹp, văn hóa một vùng đất hoặc những tộc người.

Sau nhiều năm nghiên cứu. Gami Theme Park là chủ đầu tư Việt Nam duy nhất táo bạo đầu tư thuê cha đẻ của ngành nghệ thuật thực cảnh thế giới mang công nghệ hàng đầu về Việt Nam để chế tác và dàn dựng một Ký Ức Hội An tầm vóc, quy mô mà không nhiều quốc gia làm được.

Kho chất liệu văn hóa truyền thống của địa phương được tư vấn cung cấp bởi các nhà nghiên cứu lớn của Việt Nam như: ông Đức Trịnh - Phó Chủ tịch Hội nhạc sỹ Việt Nam; kiến trúc sư Hoàng Đạo Kính - Ủy viên Hội đồng di sản văn hóa quốc gia, Phó Chủ tịch Hội kiến trúc sư Việt Nam, Chủ tịch hội đồng kiến trúc; nhà sử học Dương Trung Quốc và họa sỹ Trịnh Quang Vũ - nhà nghiên cứu sử học và mỹ thuật Việt Nam.

Vị đạo diễn Trung Quốc - ông Mai Soái Nguyên đã sử dụng công nghệ dàn dựng tầm vóc quốc tế nhằm tạo nên một Ký ức Hội An quy mô, chinh phục khán giả trong và ngoài nước. 

Kí ức Hội An quy tụ sự tham gia của 500 diễn viên chuyên nghiệp ba miền.
Kí ức Hội An quy tụ sự tham gia của 500 diễn viên chuyên nghiệp ba miền.

Thông qua các chuyến khảo sát thực địa của giám đốc hơn 200 hãng lữ hành trong nước và quốc tế, Ký ức Hội An được đưa vào hàng loạt tour du lịch trọn gói và cố định cho du khách khi tới Hội An, đồng thời quảng bá trên các kênh thông tin du lịch trong nước và quốc tế.

Theo đại diện Gami Theme Park, sau khi nhận được các hình ảnh của show diễn, đơn vị quản lý kênh truyền thông đã rà soát nhận xét của du khách trên các kênh thông tin như TripAdvisor, báo chí địa phương, lãnh đạo và người dân... Các mạng lưới thông tin bản địa các kênh truyền thông cũng đến trực tiếp Công viên văn hóa chủ đề Ấn tượng Hội An để trải nghiệm, thu thập ý kiến đánh giá của du khách.

Sau khi thẩm định về tính xác thực của thông tin, ban thẩm định lựa chọn đăng tải Ký ức Hội An tại Quảng trường Thời Đại (Times Square) - biểu tượng của New York ngày 14/3 vừa qua.

Show diễn được chiếu trên quảng trường Thời đại ngày 14/3.
Show diễn được chiếu trên quảng trường Thời đại ngày 14/3.

"Điều tự hào nhất, khán giả thế giới có thể so sánh Ký ức Hội An về quy mô với những chương trình biểu diễn tại Trung Quốc và về tính nghệ thuật với chương trình tại các quốc gia nổi tiếng về nghệ thuật, sân khấu như Italy, Pháp, Mỹ...", đại diện nhà sản xuất nói. 

Sau khi hình ảnh show diễn được trình chiếu tại Times Square, New York và nhận được những đánh giá cao của du khách, "show đẹp nhất thế giới" và "Bữa tiệc thị giác" là những điều nhiều khán giả mô tả khi xem show diễn nổi bật về văn hóa Việt Nam.

Vài ngày sau sự kiện, nhiều trang tin tức liên hệ với Gami Theme Park để tìm hiểu thông tin về show diễn và đăng tải. 220 hãng thông tấn báo chí từ Mỹ, Nhật Bản, Hàn Quốc, Ấn Độ, Trung Quốc, Singapore... đã nhanh chóng lan tỏa thông tin về chương trình.

Mục tiêu 2 triệu du khách trong năm 2019

Show diễn được Tổng cục trưởng Tổng cục du lịch đánh giá là sản phẩm du lịch sáng tạo mới tiêu biểu của năm 2018 và đưa Ký ức Hội An vào các chương trình xúc tiến du lịch trên các thị trường quốc tế.

Ông Thomas Schuller-Gotzburg - đại sứ Áo tại Việt Nam bày tỏ sự ấn tượng khi show diễn kể câu chuyện về văn hóa - lịch sử của Hội An nói riêng và Việt Nam nói chung chỉ trong vòng một tiếng đồng hồ, thông qua vũ đạo, trang phục và số lượng diễn viên đông đảo. Theo ông, Ký ức Hội An là show diễn mà mọi người nên thưởng thức khi đến với thành phố này.

Theo đại diện Gami Theme Park, show diễn vẫn đang nhận liên hệ thông tin của các hãng thông tấn báo chí uy tín thế giới như CNN và BBC. Nhà sản xuất tin tưởng, hiệu ứng truyền thông vẫn sẽ tiếp tục lan tỏa trên thế giới.

Với mục tiêu đón 2 triệu khách năm 2019, đơn vị sản xuất dự kiến tăng diễn lên hai suất mỗi ngày từ cuối tháng 4/2019 và không ngừng cải tiến, nâng cấp show mỗi 6 tháng.  Phiên bản mới của show diễn - Ký ức Hội An 3 sẽ sớm ra mắt với sự nâng cấp tối tân hệ thống âm thanh, ánh sáng.

Cùng với đó, đơn vị dự kiến mang Ký ức Hội An tới các thị trường khó tính như Mỹ, châu Âu, Australia, Nhật Bản, Hàn Quốc, mang câu chuyện Việt kể với thế giới. Ngoài Ký ức Hội An, Gami Theme Park dự định hoàn thiện mô hình và nhân rộng tại các vùng có chiều sâu văn hóa lịch sử trên khắp đất nước mà sắp tới là Hà Nội và Khu vực Tây Nguyên.

Hà Trương